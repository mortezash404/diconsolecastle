﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CastleDIConsoleApp.Services;

namespace CastleDIConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new WindsorContainer();

            container.Register(Component.For<ITestService>().ImplementedBy<TestService>());

            var root = container.Resolve<ITestService>();

            root.SayHello();
        }
    }
}
